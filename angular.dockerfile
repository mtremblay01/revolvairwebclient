FROM node:latest

RUN apt-get update -q -y 

RUN npm install --global  @angular/cli@1.6.5

WORKDIR /home/app

COPY package*.json /home/app/

RUN npm install 

COPY ./ /home/app/

RUN ng build --prod --aot false

CMD tail -f /dev/null
