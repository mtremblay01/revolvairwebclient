import {TestBed, inject, fakeAsync, async} from '@angular/core/testing';

import { StationService } from './station.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';
import {environment} from '../../../environments/environment';
import {Station} from '../../models/station';

describe('StationServiceService', () => {

  let stationService: StationService;
  const apiUrl = environment.apiUrl;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ],
      providers: [StationService]
    });
    stationService = TestBed.get(StationService);
  });

  it('should be created', inject([StationService], (service: StationService) => {
    expect(service).toBeTruthy();
  }));

  describe('getStations()', () => {
    it('should get the stations from apiUrl', async(
      inject([HttpClient, HttpTestingController],
        (http: HttpClient, backend: HttpTestingController) => {
          //Arrange
          let expectedUrl = apiUrl+"/stations";

          //Act
          stationService.getStations().subscribe();

          //Assert
          backend.expectOne({
            url: expectedUrl,
            method: 'GET'
          });
        })));

    it('should return an empty array if there was an error', async(
      inject([HttpClient, HttpTestingController],
        (http: HttpClient, backend: HttpTestingController) => {
          //Arrange
          let expectedValue = [];
          let actualValue : Array<any>;
          spyOn(http, 'get').and.returnValue(Observable.throw("oh no!"));

          //Act
          stationService.getStations().subscribe(
            stations => {
              actualValue = stations;
              expect(actualValue).toEqual(expectedValue);
            }
          );
        })))
  });

  describe('getStation()', () => {
    it('should get the station from apiUrl', async(
      inject([HttpClient, HttpTestingController],
        (http: HttpClient, backend: HttpTestingController) => {
          //Arrange
          let stationId = 9000;
          let expectedUrl = apiUrl+"/stations/"+stationId;

          //Act
          stationService.getStation(stationId).subscribe();

          //Assert
          backend.expectOne({
            url: expectedUrl,
            method: 'GET'
          })
        })));

    it('should return a new station if there was an error', async(
      inject([HttpClient, HttpTestingController],
        (http: HttpClient, backend: HttpTestingController) => {
          //Arrange
          let expectedValue = new Station;
          let actualValue : Station;
          let stationId = 9000;
          spyOn(http, 'get').and.returnValue(Observable.throw("oh no!"));

          //Act
          stationService.getStation(stationId).subscribe(
            station => {
              actualValue = station;
              expect(actualValue).toEqual(expectedValue);
            }
          );
        })))
  });

  describe('getLatestAqi()', () => {
    it('should get the latestAqi from apiUrl', async(
      inject([HttpClient, HttpTestingController],
        (http: HttpClient, backend: HttpTestingController) => {
          //Arrange
          let stationId = 9000;
          let expectedUrl = apiUrl+"/stations/"+stationId+"/sensors/latest-aqi";

          //Act
          stationService.getLatestAqi(stationId).subscribe();

          //Assert
          backend.expectOne({
            url: expectedUrl,
            method: 'GET'
          })
        })));

    it('should return a new station if there was an error', async(
      inject([HttpClient, HttpTestingController],
        (http: HttpClient, backend: HttpTestingController) => {
          //Arrange
          let expectedValue = [];
          let actualValue : number[];
          let stationId = 9000;
          spyOn(http, 'get').and.returnValue(Observable.throw("oh no!"));

          //Act
          stationService.getLatestAqi(stationId).subscribe(
            station => {
              actualValue = station;
              expect(actualValue).toEqual(expectedValue);
            }
          );
        })))
  })
});
