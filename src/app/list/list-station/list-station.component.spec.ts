import {async, ComponentFixture, fakeAsync, inject, TestBed, tick} from '@angular/core/testing';

import { ListStationComponent } from './list-station.component';
import {StationService} from '../../services/station-service/station.service';
import {Station} from '../../models/station';
import {NgxPaginationModule} from 'ngx-pagination';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {RouterTestingModule} from '@angular/router/testing';
import {AqiComponent} from '../../aqi/aqi.component';

describe('ListStationComponent', () => {
  let component: ListStationComponent;
  let listeStation = [];
  const stationId = 900;
  let fixture: ComponentFixture<ListStationComponent>;
  let stationService : StationService;
  let station : Station;

  beforeEach(async(() => {
    station = new Station();
    station.id = stationId;
    listeStation.push(station);

    TestBed.configureTestingModule({
      declarations: [
        ListStationComponent,
        AqiComponent
      ],
      providers: [
        StationService,
      ],
      imports: [
        NgxPaginationModule,
        HttpClientTestingModule,
        RouterTestingModule
      ]
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(ListStationComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    stationService = TestBed.get(StationService);
  }));

  afterEach(async(() => {
    component = null;
    listeStation = [];
    fixture.destroy();
    stationService = null;
    station = null;
  }));

  it('should create', async(() => {
    fixture.whenStable().then(() => {
      expect(component).toBeTruthy();
    });
  }));

  describe('ngOnInit()', () => {
    it('should get stations from stationService', fakeAsync(() => {
      //Arrange
      spyOn(stationService, 'getStations').and.returnValue(Observable.of(listeStation));
      tick();

      //Act
      component.ngOnInit();
      tick();

      //Assert
      expect(stationService.getStations).toHaveBeenCalled();
    }))
  });

  describe('pageChanged(page : number)', () => {
    it('should change the current page', fakeAsync(() => {
      //Arrange
      let oldPage = 1;
      component.currentPage = oldPage;
      let newPage = 2;

      //Act
      component.pageChanged(newPage);

      //Assert
      expect(component.currentPage).toBe(newPage);
    }))
  });

  describe('details(id: number)', () => {
    it('should go to details page', fakeAsync(() => {
      //Arrange
      let expected = ['detail-station', station.id];
      let spy = spyOn((<any>component).router, 'navigate');
      tick();

      //Act
      component.details(station.id);
      tick();

      //Assert
      expect(spy).toHaveBeenCalledWith(expected);
    }))
  });
});
