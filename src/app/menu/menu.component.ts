import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})

export class MenuComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  public gotoMap(){
      this.router.navigate([""]);
  }

  public gotoSignIn(){
      this.router.navigate(["sign-in"]);
  }

  public gotoSignUp(){
      this.router.navigate(["sign-up"]);
  }

  public gotoSubscribe(){
    this.router.navigate(["subscribe"]);
  }
}
