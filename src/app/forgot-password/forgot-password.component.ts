import { Component, OnInit } from '@angular/core';
import {UserService} from '../services/user-service/user.service';
import {User} from '../models/user';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  message: string;
  model = new User('', '');

  constructor(private userService : UserService) { }

  ngOnInit() {

  }

  onSubmit() {
      if (this.model.email == "") {
          return;
      }

      this.message = "Chargement...";

      this.userService.forgotPassword(this.model.email).subscribe(
          res => this.handleResponse(res)
      );
  }

  private handleResponse(res){
    if (res.error) {
      this.message = "Adresse courriel introuvable";
      return;
    }

    this.message = "Vous allez recevoir un courriel contenant un lien sous peu.";
  }
}
